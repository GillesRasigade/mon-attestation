# Mon attestation

100% based on [LAB-MI/attestation-deplacement-derogatoire-q4-2020](https://github.com/LAB-MI/attestation-deplacement-derogatoire-q4-2020) Github repository. This
app is offering a one-click option to generate your Covid19 French attestation
while keeping your personal information safe and available for later usage.

## Installation

```shell
git clone git@gitlab.com:GillesRasigade/mon-attestation.git
cd mon-attestation

git submodule update --init --recursive
npm install

npm run dev
```
